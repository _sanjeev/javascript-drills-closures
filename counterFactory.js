function counterFactory () {
    let x = 0;
    
    function incrementDecrement () {
        return obj = {
            increment : () => {
                return x += 1;
            },
            decrement : () => {
                return x -= 1;
            },
        } 
    }
    return incrementDecrement();
}

module.exports = counterFactory;