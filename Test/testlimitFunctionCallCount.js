const limitFunctionCallCount = require ('./../limitFunctionCallCount.js');
function cb () {
    console.log ('cb invoked');
}
let result = limitFunctionCallCount (cb, 3);
result.invoke();
result.invoke();
result.invoke();
result.invoke();