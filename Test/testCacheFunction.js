const cacheFunction = require ('./../cacheFunction.js');

const cb = (name) => {
    console.log ('First time cb is invoked');
    return name;
}

let res = cacheFunction (cb);
console.log (res.innerFunction('sahil'));
console.log (res.innerFunction('sahil'));
