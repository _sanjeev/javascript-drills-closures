// function cacheFunction (cb) {
//     let cache = [];
//     function innerFunction (n) {
//         if (cache[n] === undefined) {
//             //Sum of number from 1 to n
//             cb (n);
//             let res = 0;
//             for (let index = 1; index <= n; index++) {
//                 res = res + index;
//             }
//             cache[n] = res;
//         } else {
//             return cache[n];
//         }
//     }
//     return {innerFunction};
// }

//Instead of using array i am using object for storing the string in the form of key value pair.

function cacheFunction (cb) {
    let cache = {};
    function innerFunction(n) {
        if (cache[n]) {
            return cache[n];
        } else {
            const res = cb (n);
            cache[n] = res;
            return cache[n];
            //return res;
        }
    }
    return {innerFunction};
}

module.exports = cacheFunction;