function limitFunctionCallCount (cb, n) {

    let callingFunction = n;

    function invoke () {
        if (callingFunction > 0) {
            cb ();
            callingFunction--;
        } else {
            console.log (n + 'times invoked');
        }
    }
    return {invoke};
}

module.exports = limitFunctionCallCount;

